import { addressInformationType } from "./types";

export const addressAdd = data => ({
  type: addressInformationType.ADD,
  data
});
export const addressRemove = id => ({
  type: addressInformationType.REMOVE,
  id
});
