import { vehiclesType } from "./types";

export const vehicleAdd = data => ({
  type: vehiclesType.ADD,
  data
});
export const vehicleRemove = id => ({
  type: vehiclesType.REMOVE,
  id
});
