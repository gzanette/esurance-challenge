import { personalInformationType } from "./types";

export const personAdd = data => ({
  type: personalInformationType.ADD,
  data
});
export const personRemove = id => ({
  type: personalInformationType.REMOVE,
  id
});
export const personSetPrimary = id => ({
  type: personalInformationType.SET_PRIMARY,
  id
});
