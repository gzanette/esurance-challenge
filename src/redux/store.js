import { applyMiddleware, compose, createStore } from 'redux';
import monitorReducersEnhancer from './enhancers/monitorReducers';
import loggerMiddleware from './middlewares/logger';
import rootReducer from './reducers';

console.log('ENV=', process.env.NODE_ENV);

export default function configureStore(preloadedState) {
  const middlewares = [];
  if (process.env.NODE_ENV === 'development') {
    middlewares.push(loggerMiddleware);
  }
  const middlewareEnhancer = applyMiddleware(...middlewares);

  const enhancers = [middlewareEnhancer];
  if (process.env.NODE_ENV === 'development') {
    enhancers.push(monitorReducersEnhancer);
  }
  const composedEnhancers = compose(...enhancers);

  const store = createStore(rootReducer, preloadedState, composedEnhancers);

  return store;
}
