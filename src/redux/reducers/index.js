import { combineReducers } from 'redux';
import personalInformationReducer from './personalInformationReducer';
import addressInformationReducer from './addressInformationReducer';
import vehiclesReducer from './vehiclesReducer';

export default combineReducers({
  personalInformation: personalInformationReducer,
  addressInformation: addressInformationReducer,
  vehicles: vehiclesReducer,
});
