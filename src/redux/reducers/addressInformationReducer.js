import { addressInformationType } from '../actions/types';

export default (state = [], action) => {
  const { type, data, id } = action;

  switch (type) {
    case addressInformationType.ADD: {
      return [...state, data];
    }
    case addressInformationType.REMOVE: {
      const newState = state.slice();
      const removeItemIndex = newState.findIndex(item => item.id === id);

      if (removeItemIndex >= 0) {
        newState.splice(removeItemIndex, 1);

        return newState;
      }

      return state;
    }
    default:
      return state;
  }
};
