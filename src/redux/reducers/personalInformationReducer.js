import { personalInformationType } from '../actions/types';

const resetPrimary = state => state.map(item => ({ ...item, primary: false }));

export default (state = [], action) => {
  const { type, data, id } = action;

  switch (type) {
    case personalInformationType.ADD: {
      let newState = state;

      if (data.primary) {
        newState = resetPrimary(state);
      }

      return [...newState, data];
    }
    case personalInformationType.REMOVE: {
      const newState = state.slice();
      const removeItemIndex = newState.findIndex(item => item.id === id);

      if (removeItemIndex >= 0) {
        newState.splice(removeItemIndex, 1);

        if (newState.length === 1) {
          return newState.map(item => ({ ...item, primary: true }));
        }

        return newState;
      }

      return state;
    }
    case personalInformationType.SET_PRIMARY: {
      let newState = state.slice();
      const selectedIndex = newState.findIndex(item => item.id === id);

      if (selectedIndex >= 0) {
        newState = resetPrimary(state);
        newState[selectedIndex] = { ...newState[selectedIndex], primary: true };
      }

      return newState;
    }
    default:
      return state;
  }
};
