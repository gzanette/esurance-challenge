import { vehiclesType } from '../actions/types';

export default (state = [], action) => {
  const { type, data, id } = action;

  switch (type) {
    case vehiclesType.ADD:
      return [...state, data];
    case vehiclesType.REMOVE: {
      const newState = state.slice();
      const removeItemIndex = newState.findIndex(item => item.id === id);

      if (removeItemIndex >= 0) {
        newState.splice(removeItemIndex, 1);

        return newState;
      }

      return state;
    }
    default:
      return state;
  }
};
