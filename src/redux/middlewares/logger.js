const logger = store => next => (action) => {
  /* eslint-disable */
  console.group(action.type);
  console.info('dispatching', action);
  let result = next(action);
  console.log('next state', store.getState());
  console.groupEnd();
  /* eslint-enable */

  return result;
};

export default logger;
