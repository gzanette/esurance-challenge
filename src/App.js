import React from 'react';
import { Provider } from 'react-redux';
import { withRouter } from 'react-router';
import { HashRouter as Router } from 'react-router-dom';
import Routes from './routes';
import TheHeader from './components/TheHeader';
import configureStore from './redux/store';
import './scss/main.scss';

const store = configureStore({});
const TheHeaderWithRouter = withRouter(TheHeader);
const RoutesWithRouter = withRouter(Routes);

const App = () => (
  <Provider store={store}>
    <Router>
      <TheHeaderWithRouter />
      <main className="main-content">
        <RoutesWithRouter />
      </main>
    </Router>
  </Provider>
);

export default App;
