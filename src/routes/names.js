export const HOME = '/';
export const INFORMATION = '/information';
export const PERSONAL = `${INFORMATION}/personal`;
export const ADDRESS = `${INFORMATION}/address`;
export const VEHICLES = '/vehicles';
export const CONFIRMATION = '/confirmation';
