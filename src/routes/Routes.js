import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from '../pages/home';
import Information from '../pages/information';
import Vehicles from '../pages/vehicles';
import Confirmation from '../pages/confirmation';
import NotFound from '../pages/notFound';
import * as RouteNames from './names';

export default () => (
  <Switch>
    <Route path={RouteNames.HOME} component={Home} exact />
    <Route path={`${RouteNames.INFORMATION}/:step`} component={Information} exact />
    <Route path={`${RouteNames.VEHICLES}`} component={Vehicles} exact />
    <Route path={`${RouteNames.CONFIRMATION}`} component={Confirmation} exact />
    <Route path="/*" component={NotFound} />
  </Switch>
);
