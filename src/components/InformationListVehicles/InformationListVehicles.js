import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { vehicleRemove } from '../../redux/actions/vehiclesAction';
import InformationList from '../InformationList';

const InformationListVehicles = ({ edit }) => {
  const dispatch = useDispatch();
  const list = useSelector(state => state.vehicles);
  const onRemove = id => dispatch(vehicleRemove(id));

  const inforList = list.map(item => ({
    id: item.id,
    title: item.model,
    subtitle: item.year,
    descriptionList: [
      { title: 'Make:', text: item.make },
      { title: 'Type:', text: item.type },
    ],
  }))

  return (
    <InformationList
      list={inforList}
      onRemove={edit ? (item) => onRemove(item.id) : null}
    />
  )
}

export default InformationListVehicles;
