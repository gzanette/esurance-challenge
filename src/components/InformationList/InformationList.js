import React from 'react';

const InformationList = ({
  list,
  onRemove,
  onEdit,
  onSetPrimary,
}) => {
  if (!list || !list.length) {
    return null;
  }

  return (
    <ul className="row list-unstyled align-items-stretch">
      {list.map(item => (
        <li
          key={`information-${item.id}`}
          className="col-3"
        >
          <div className={`card h-100${item.primary ? ' border-primary' : ''}`}>
            <div className="card-body">
              {!!item.title && (
                <h5 className="card-title">
                  {item.title}
                </h5>
              )}
              {!!item.subtitle && (
                <h6 className="card-subtitle mb-2 text-muted">
                  {item.subtitle}
                </h6>
              )}

              {!!(item.descriptionList && item.descriptionList.length) && (
                <dl className="d-block mb-3">
                  {item.descriptionList.map((item, index) => (
                    <div key={`description-list-${index}`}>
                      <dt className="d-inline mr-2">
                        {item.title}
                      </dt>
                      <dd className="d-inline">
                        {item.text}
                      </dd>
                    </div>
                  ))}
                </dl>
              )}

              {onEdit && (
                <button
                  type="button"
                  className="btn btn-sm btn-primary mr-3"
                  onClick={() => onEdit(item)}
                >
                  Edit
                </button>
              )}
              {(onSetPrimary && !item.primary) && (
                <button
                  type="button"
                  className="btn btn-sm btn-primary mr-3"
                  onClick={() => onSetPrimary(item)}
                >
                  Set as primary
                </button>
              )}
              {onRemove && (
                <button
                  type="button"
                  className="btn btn-sm btn-light"
                  onClick={() => onRemove(item)}
                >
                  Remove
                </button>
              )}
            </div>
          </div>
        </li>
      ))}
    </ul>
  )
}

export default InformationList;
