import React, { useState } from 'react';
import Form from '../Form';
import FormGroup from '../FormGroup';
import { RULES } from './rules';

const InformationFormPersonal = ({
  onSuccess = () => {},
  onCancel,
}) => {
  const [errors, setErrors] = useState([]);
  const fields = RULES;
  const onValidationForm = (errs) => {
    const newErrors = errs.map((err, index) => {
      const errorIndex = err ? err.findIndex(isValid => isValid) : -1;

      return (errorIndex >= 0) ? fields[index].messages[errorIndex] : null;
    });

    setErrors(newErrors);
  };
  const onSuccessForm = (data) => {
    const formData = { ...data, id: Math.round(Math.random() * 9999) };

    onSuccess(formData);
  };

  return (
    <Form
      id="personal-info-form"
      fields={fields}
      onSuccess={onSuccessForm}
      onValidation={onValidationForm}
      onCancel={onCancel}
    >
      <div className="row">
        <div className="col-6">
          <FormGroup
            label="First Name *"
            id="personal-info-first-name"
            name="firstName"
            error={errors[0]}
          />
        </div>
        <div className="col-6">
          <FormGroup
            label="Last Name *"
            id="personal-info-last-name"
            name="lastName"
            error={errors[1]}
          />
        </div>
      </div>
      <div className="row">
        <div className="col-3">
          <FormGroup
            label="Phone Number *"
            id="personal-info-phone-number"
            name="phoneNumber"
            inputProps={({
              type: "tel",
              maxLength: 14,
            })}
            error={errors[2]}
          />
        </div>
        <div className="col-3">
          <FormGroup
            label="Driving License *"
            id="personal-info-license"
            name="license"
            error={errors[3]}
          />
        </div>
        <div className="col-3">
          <div className="form-group">
            <label htmlFor="personal-info-gender">Gender *</label>
            <select
              className={`form-control ${!!errors[4] ? 'border-danger' : ''}`}
              id="personal-info-gender"
              name="gender"
              defaultValue="Select"
            >
              <option disabled>Select</option>
              <option>Male</option>
              <option>Female</option>
              <option>Other</option>
            </select>
            {!!errors[4] && (
              <small className="text-danger">{errors[4]}</small>
            )}
          </div>
        </div>
      </div>
      <FormGroup
        label="Education *"
        id="personal-info-education"
        name="education"
        error={errors[5]}
      />

      <div className="form-group form-check">
        <input
          type="checkbox"
          className="form-check-input"
          id="personal-info-primary"
          name="primary"
        />
        <label
          className="form-check-label"
          htmlFor="personal-info-primary"
        >
          Is primary?
        </label>
      </div>
    </Form>
  );
}

export default InformationFormPersonal;
