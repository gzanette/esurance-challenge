const requiredRul = { name: 'required' };

export const RULES = [
  {
    name: 'firstName',
    rules: [requiredRul],
    messages: ['First Name is required']
  },
  {
    name: 'lastName',
    rules: [requiredRul],
    messages: ['Last Name is required']
  },
  {
    name: 'phoneNumber',
    rules: [
      requiredRul,
      { name: 'minLength', value: 10 },
      { name: 'phone' },
    ],
    messages: [
      'Phone is required',
      'You need to fill a proper phone number',
      'Your phone number is not right',
    ]
  },
  {
    name: 'license',
    rules: [requiredRul],
    messages: ['License is required']
  },
  {
    name: 'gender',
    rules: [requiredRul],
    messages: ['Gender is required']
  },
  {
    name: 'education',
    rules: [requiredRul],
    messages: ['Education is required']
  },
  {
    name: 'primary'
  },
];
