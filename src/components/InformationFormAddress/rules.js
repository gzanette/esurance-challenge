const requiredRul = { name: 'required' };

export const RULES = [
  {
    name: 'line1',
    rules: [requiredRul],
    messages: ['Address line 1 is required']
  },
  {
    name: 'line2'
  },
  {
    name: 'city',
    rules: [requiredRul],
    messages: ['City is required']
  },
  {
    name: 'state',
    rules: [requiredRul],
    messages: ['State is required']
  },
  {
    name: 'zip',
    rules: [
      requiredRul,
      { name: 'minLength', value: 5 },
      { name: 'zipcode' },
    ],
    messages: [
      'Zipcode is required',
      'You need a zipcode with 5 digits',
      'You need a valid zipcode',
    ]
  },
  {
    name: 'isBilling'
  },
];
