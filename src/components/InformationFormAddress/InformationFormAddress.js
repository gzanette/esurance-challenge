import React, { useState } from 'react';
import Form from '../Form';
import FormGroup from '../FormGroup';
import { RULES } from './rules';

const InformationFormAddress = ({
  onSuccess = () => {},
  onCancel,
}) => {
  const [errors, setErrors] = useState([]);
  const fields = RULES;
  const onValidationForm = (errs) => {
    const newErrors = errs.map((err, index) => {
      const errorIndex = err ? err.findIndex(isValid => isValid) : -1;

      return (errorIndex >= 0) ? fields[index].messages[errorIndex] : null;
    });

    setErrors(newErrors);
  };
  const onSuccessForm = (data) => {
    const formData = { ...data, id: Math.round(Math.random() * 9999) };

    onSuccess(formData);
  };

  return (
    <Form
      id="address-info-form"
      fields={fields}
      onSuccess={onSuccessForm}
      onValidation={onValidationForm}
      onCancel={onCancel}
    >
      <div className="row">
        <div className="col-6">
          <FormGroup
            label="Address line 1 *"
            id="address-info-line-1"
            name="line1"
            error={errors[0]}
          />
        </div>
        <div className="col-6">
          <FormGroup
            label="Address line 2"
            id="address-info-line-2"
            name="line2"
            error={errors[1]}
          />
        </div>
      </div>
      <div className="row">
        <div className="col-3">
          <FormGroup
            label="City *"
            id="address-info-city"
            name="city"
            error={errors[2]}
          />
        </div>
        <div className="col-3">
          <FormGroup
            label="State *"
            id="address-info-state"
            name="state"
            error={errors[3]}
          />
        </div>
        <div className="col-3">
          <FormGroup
            label="Zip *"
            id="address-info-zip"
            name="zip"
            inputProps={({
              maxLength: 5
            })}
            error={errors[4]}
          />
        </div>
      </div>
      <div className="form-group form-check">
        <input
          type="checkbox"
          className="form-check-input"
          id="address-info-billing"
          name="isBilling"
        />
        <label
          className="form-check-label"
          htmlFor="address-info-billing"
        >
          Is billing address?
        </label>
      </div>
    </Form>
  );
}

export default InformationFormAddress;
