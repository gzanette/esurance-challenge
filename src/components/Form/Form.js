import React from 'react';
import * as Rules from './validation';

const InformationFormPersonal = ({
  id = 'form-id',
  fields = [],
  children,
  onSubmit = () => {},
  onSuccess = () => {},
  onValidation = () => {},
  onCancel = () => {},
}) => {
  const validate = (data) => {
    return fields.map(field => {
      const rules = field.rules;

      if (!rules || !rules.length) {
        return null;
      }

      return rules.map(rule => (
        Rules[rule.name](data[field.name], rule)
      ));
    });
  };
  const onSubmitForm = (event) => {
    event.preventDefault();

    const formData = new FormData(event.currentTarget.parentElement);
    const data = fields.reduce((acc, curr) => ({ ...acc, [curr.name]: formData.get(curr.name) }), {});

    onSubmit(data);

    const hasErrors = validate(data);
    if (hasErrors.every(field => !field || field.every(f => !f))) {
      onSuccess(data);
    } else {
      onValidation(hasErrors);
    }
  }

  return (
    <form
      id={id}
      name={id}
    >
      {children}

      <button
        type="submit"
        className="btn btn-primary"
        onClick={onSubmitForm}
      >
        Submit
      </button>
      <button
        type="clean"
        className="btn btn-link text-secondary"
        onClick={onCancel}
      >
        Cancel
      </button>
    </form>
  );
}

export default InformationFormPersonal;
