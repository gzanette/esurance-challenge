export const required = (value, options) => !value;
export const maxLength = (value, options) => !value || (value.length > options.value);
export const minLength = (value, options) => !value || (value.length < options.value);
export const phone = (value, options) => !value || (value.replace(/[^0-9]/g, '').length > 10);
export const zipcode = (value, options) => !value || (value.replace(/[^0-9]/g, '').length !== 5);
