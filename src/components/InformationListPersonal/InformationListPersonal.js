import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import InformationList from '../InformationList';
import { personRemove, personSetPrimary } from '../../redux/actions/personAction';

const InformationListPersonal = ({ edit = false }) => {
  const dispatch = useDispatch();
  const list = useSelector(state => state.personalInformation);
  const onRemove = id => dispatch(personRemove(id));
  const onSetPrimary = id => dispatch(personSetPrimary(id));

  const inforList = list.map(item => ({
    id: item.id,
    primary: item.primary,
    title: `${item.firstName} ${item.lastName}`,
    subtitle: item.gender,
    descriptionList: [
      { title: 'License:', text: item.license },
      { title: 'Phone:', text: item.phoneNumber },
      { title: 'Education:', text: item.education },
    ],
  }))

  return (
    <InformationList
      list={inforList}
      onRemove={edit ? (item) => onRemove(item.id) : null}
      onSetPrimary={edit ? (item) => onSetPrimary(item.id) : null}
    />
  )
}

export default InformationListPersonal;
