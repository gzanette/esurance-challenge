import React from 'react';
import { Link } from 'react-router-dom';
import './TheHeader.scss';

const TheHeader = () => (
  <header className="bg-dark text-white main-header">
    <div className="container">
      <Link to="/">
        <img
          src="https://www.esurance.com/failover/geo/img/logo-esu-as-308.png"
          alt="Esurance car insurance"
          width="154"
          height="35"
          title="Esurance car insurance"
        />
      </Link>
    </div>
  </header>
);

export default TheHeader;
