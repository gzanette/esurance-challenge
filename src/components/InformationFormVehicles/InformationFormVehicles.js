import React, { useState } from 'react';
import Form from '../Form';
import FormGroup from '../FormGroup';
import { RULES } from './rules';

const InformationFormVehicles = ({
  onSuccess = () => {},
  onCancel,
}) => {
  const [errors, setErrors] = useState([]);
  const fields = RULES;
  const onValidationForm = (errs) => {
    const newErrors = errs.map((err, index) => {
      const errorIndex = err ? err.findIndex(isValid => isValid) : -1;

      return (errorIndex >= 0) ? fields[index].messages[errorIndex] : null;
    });

    setErrors(newErrors);
  };
  const onSuccessForm = (data) => {
    const formData = { ...data, id: Math.round(Math.random() * 9999) };

    onSuccess(formData);
  };

  return (
    <Form
      id="vehicles-form"
      fields={fields}
      onSuccess={onSuccessForm}
      onValidation={onValidationForm}
      onCancel={onCancel}
    >
      <div className="row">
        <div className="col-md-3">
          <FormGroup
            label="Year *"
            id="vehicle-year"
            name="year"
            error={errors[0]}
          />
        </div>
        <div className="col-md-3">
          <FormGroup
            label="Make *"
            id="vehicle-make"
            name="make"
            error={errors[1]}
          />
        </div>
        <div className="col-md-3">
          <FormGroup
            label="Model *"
            id="vehicle-model"
            name="model"
            error={errors[2]}
          />
        </div>
        <div className="col-md-3">
          <FormGroup
            label="Type *"
            id="vehicle-type"
            name="type"
            error={errors[3]}
          />
        </div>
      </div>
    </Form>
  );
}

export default InformationFormVehicles;
