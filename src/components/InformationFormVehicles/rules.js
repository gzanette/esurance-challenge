const requiredRul = { name: 'required' };

export const RULES = [
  {
    name: 'year',
    rules: [
      requiredRul,
      { name: 'minLength', value: 4 },
      { name: 'maxLength', value: 4 },
    ],
    messages: [
      'Year is required',
      'We need a year like this "2019"',
      'We need a year like this "2019"',
    ]
  },
  {
    name: 'make',
    rules: [requiredRul],
    messages: ['Make is required']
  },
  {
    name: 'model',
    rules: [requiredRul],
    messages: ['Model is required']
  },
  {
    name: 'type',
    rules: [requiredRul],
    messages: ['Type is required']
  },
];
