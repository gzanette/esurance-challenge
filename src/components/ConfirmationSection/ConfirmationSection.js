import React from 'react';
import { Link } from 'react-router-dom';

const ConfirmationSection = ({ title = '', edit, children }) => (
  <article className="mb-5">
    <header className="d-flex align-items-center">
      <h2>
        {title}
      </h2>

      {!!edit && (
        <Link
          to={edit}
          className="ml-3"
        >
          Edit
        </Link>
      )}
    </header>
    {children}
  </article>
);

export default ConfirmationSection;
