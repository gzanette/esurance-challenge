import React from 'react';
import './ButtonAdd.scss';

const ButtonAdd = ({ onClick, className }) => (
  <button
    type="button"
    className={`d-block btn btn-light button-add ${className}`}
    onClick={onClick}
  >
    Add new
  </button>
);

export default ButtonAdd;
