import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import InformationList from '../InformationList';
import { addressRemove } from '../../redux/actions/addresAction';

const InformationListAddress = ({ edit = false }) => {
  const dispatch = useDispatch();
  const list = useSelector(state => state.addressInformation);
  const onRemove = id => dispatch(addressRemove(id));

  if (!list || !list.length) {
    return null;
  }

  const inforList = list.map(item => ({
    id: item.id,
    primary: item.isBilling,
    title: item.isBilling ? 'BIlling Address' : 'Mailing Address',
    descriptionList: [
      { title: 'Address line 1', text: item.line1 },
      { title: 'Address line 2', text: item.line2 },
      { title: 'City', text: item.city },
      { title: 'State', text: item.state },
      { title: 'Zip', text: item.zip },
    ],
  }))

  return (
    <InformationList
      list={inforList}
      onRemove={edit ? (item) => onRemove(item.id) : null}
    />
  )
}

export default InformationListAddress;
