import React from 'react';

const FormGroup = ({
  id = 'input-id',
  name = 'input-name',
  label = '',
  inputProps = {},
  error,
}) => (
  <div className="form-group">
    <label htmlFor={id}>
      {label}
    </label>

    <input
      type="text"
      className={`form-control ${!!error ? 'border-danger' : ''}`}
      id={id}
      name={name}
      {...inputProps}
    />
    {!!error && (
      <small className="text-danger">{error}</small>
    )}
  </div>
);

export default FormGroup;
