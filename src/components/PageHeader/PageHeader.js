import React from 'react';
import { Link } from 'react-router-dom';

const PageHeader = ({ title, description, action }) => (
  <div className="page-header mb-5 d-flex justify-content-between align-items-start">
    <div>
      {!!title && (
        <h1 className="text-capitalize">{title}</h1>
      )}
      {!!description && (
        <p>{description}</p>
      )}
    </div>
    {!!action && (
      <Link
        className="btn btn-primary mt-2"
        to={action}
      >
        Continue
      </Link>
    )}
  </div>
);

export default PageHeader;
