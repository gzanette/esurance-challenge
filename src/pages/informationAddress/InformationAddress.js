import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import InformationFormAddress from '../../components/InformationFormAddress';
import ButtonAdd from '../../components/ButtonAdd';
import InformationListAddress from '../../components/InformationListAddress';
import { addressAdd } from '../../redux/actions/addresAction';

const InformationAddress = () => {
  const [adding, setAdding] = useState(false);
  const addressList = useSelector(state => state.addressInformation);
  const dispatch = useDispatch();
  const canAdd = addressList.length < 2;
  const onFormSuccess = data => {
    dispatch(
      addressAdd(data)
    );
    if (!addressList.length && data.isBilling) {
      dispatch(
        addressAdd({ ...data, id: Math.round(Math.random() * 99999), isBilling: false })
      );
    }
    setAdding(false);
  };

  return (
    <>
      {(!adding && canAdd) && (
        <ButtonAdd
          className="mb-5"
          onClick={() => setAdding(true)}
        />
      )}

      {(adding && canAdd) && (
        <div className="bg-light rounded p-4 mb-5">
          <InformationFormAddress
            onSuccess={onFormSuccess}
            onCancel={() => setAdding(false)}
          />
        </div>
      )}

      <InformationListAddress edit />
    </>
  );
};

export default InformationAddress;
