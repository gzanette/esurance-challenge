import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PageHeader from '../../components/PageHeader';
import InformationFormVehicles from '../../components/InformationFormVehicles';
import InformationListVehicles from '../../components/InformationListVehicles';
import ButtonAdd from '../../components/ButtonAdd';
import { vehicleAdd } from '../../redux/actions/vehiclesAction';
import { CONFIRMATION } from '../../routes/names';

const Vehicles = () => {
  const dispatch = useDispatch();
  const vehicles = useSelector(state => state.vehicles);
  const [adding, setAdding] = useState(false);
  const onFormSuccess = data => {
    const isFirst = !vehicles || !vehicles.length;

    dispatch(
      vehicleAdd({
        ...data,
        primary: isFirst ? true : data.primary
      })
    );
    setAdding(false);
  };

  return (
    <section className="container">
      <PageHeader
        title="Vehicles"
        description="Please add your vehicles"
        action={CONFIRMATION}
      />

      {!adding && (
        <ButtonAdd
          className="mb-5"
          onClick={() => setAdding(true)}
        />
      )}

      {adding && (
        <div className="bg-light rounded p-4 mb-5">
          <InformationFormVehicles
            onSuccess={onFormSuccess}
            onCancel={() => setAdding(false)}
          />
        </div>
      )}

      <InformationListVehicles edit />
    </section>
  );
};

export default Vehicles;
