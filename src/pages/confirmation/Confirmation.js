import React from 'react';
import PageHeader from '../../components/PageHeader';
import InformationListPersonal from '../../components/InformationListPersonal';
import InformationListVehicles from '../../components/InformationListVehicles';
import ConfirmationSection from '../../components/ConfirmationSection';
import InformationListAddress from '../../components/InformationListAddress';
import { VEHICLES, PERSONAL, ADDRESS } from '../../routes/names';

const Confirmation = () => {
  return (
    <section className="container">
      <PageHeader
        title="Confirmation"
        description="Please take a look at your data"
      />

      {/* Personal Information */}
      <ConfirmationSection
        title="Personal Information"
        edit={PERSONAL}
      >
        <InformationListPersonal />
      </ConfirmationSection>

      <ConfirmationSection
        title="Address Information"
        edit={ADDRESS}
      >
        <InformationListAddress />
      </ConfirmationSection>

      {/* Vehicles */}
      <ConfirmationSection
        title="Vehicles Information"
        edit={VEHICLES}
      >
        <InformationListVehicles />
      </ConfirmationSection>
    </section>
  );
};

export default Confirmation;
