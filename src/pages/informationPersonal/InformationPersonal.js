import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import InformationFormPersonal from '../../components/InformationFormPersonal';
import InformationListPersonal from '../../components/InformationListPersonal';
import ButtonAdd from '../../components/ButtonAdd';
import { personAdd } from '../../redux/actions/personAction';

const InformationPersonal = () => {
  const [adding, setAdding] = useState(false);
  const dispatch = useDispatch();
  const personalInformation = useSelector(state => state.personalInformation);
  const onFormSuccess = data => {
    const isFirst = !personalInformation || !personalInformation.length;

    dispatch(
      personAdd({
        ...data,
        primary: isFirst ? true : data.primary,
      })
    );
    setAdding(false);
  };

  return (
    <>
      {!adding && (
        <ButtonAdd
          className="mb-5"
          onClick={() => setAdding(true)}
        />
      )}

      {adding && (
        <div className="bg-light rounded p-4 mb-5">
          <InformationFormPersonal
            onSuccess={onFormSuccess}
            onCancel={() => setAdding(false)}
          />
        </div>
      )}

      <InformationListPersonal edit />
    </>
  );
};

export default InformationPersonal;
