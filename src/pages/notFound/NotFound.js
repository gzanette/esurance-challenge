import React from 'react';

const NotFound = () => (
  <div className="container">
    Ups, we couldn't find this page
  </div>
);

export default NotFound;
