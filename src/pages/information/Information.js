import React from 'react';
import { useParams } from 'react-router';
import PageHeader from '../../components/PageHeader';
import InformationPersonal from '../informationPersonal';
import InformationAddress from '../informationAddress';
import { ADDRESS, VEHICLES } from '../../routes/names';

const Information = () => {
  const { step } = useParams();
  const nextPage = step === 'personal' ? ADDRESS : VEHICLES;
  const header = {
    personal: {
      title: 'Personal Information',
      description: 'Please add your personal information',
    },
    address: {
      title: 'Address Information',
      description: 'Please add your address',
    },
  };

  return (
    <section className="container personal-page">
      <PageHeader
        title={header[step].title}
        description={header[step].description}
        action={nextPage}
      />

      {(step === 'personal') && (
        <InformationPersonal />
      )}
      {(step === 'address') && (
        <InformationAddress />
      )}
    </section>
  );
};

export default Information;
