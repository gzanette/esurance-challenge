import React from 'react';
import { Link } from 'react-router-dom';
import PageHeader from '../../components/PageHeader';
import { PERSONAL } from '../../routes/names';

const Home = () => (
  <div className="container">
    <PageHeader
      title="Welcome to eSurance"
      description="Let's fill some forms"
    />

    <Link to={PERSONAL} className="btn btn-primary">
      shall we?
    </Link>
  </div>
);

export default Home;
